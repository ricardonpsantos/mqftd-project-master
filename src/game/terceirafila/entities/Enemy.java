package game.terceirafila.entities;


import game.terceirafila.Game;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Enemy extends Entity {

    private int health;
    private boolean dead;
    private Rectangle enemyHitBox;
    private Game game;
    private Picture enemy1;

    //--------------------------------MOVEMENT BOOLEAN PROPERTIES-------------------------------------------------------

    private boolean up;
    private boolean down;
    private boolean right;
    private boolean left;

    //------------------------------------COLLISION BOOLEAN PROPERTIES--------------------------------------------------

    private boolean isCollidingUP;
    private boolean isCollidingDOWN;
    private boolean isCollidingLEFT;
    private boolean isCollidingRIGHT;

    //------------------------------------------END OF PROPERTIES-------------------------------------------------------

    public Enemy(int health, Game game) {
        this.health = health;
        this.dead = false;
        this.game = game;
        this.dead = false;
    }

    public void enemyDraw(int spawnX, int spawnY) {
        enemyHitBox = new Rectangle(spawnX, spawnY, 64, 64);
        this.enemy1 = new Picture(spawnX,spawnY, "resources/monster.gif");
        enemy1.draw();
    }

    public void getHit(int damage){
        health -= damage;

        if (health <= 0){
            die();
        }
    }

    public void die(){
        enemyHitBox.delete();
        enemy1.delete();
        dead = true;
    }

    public void move() {
        // TODO: 10/10/2019 Write enemy code (all enemies need a game, and Game can return the hero, from it we can getHitBox)

        //Get player position
        int playerXPos = game.getPlayer().getHitBox().getX();
        int playerYPos = game.getPlayer().getHitBox().getY();

        //---------ENEMY MOVEMENT TOWARDS PLAYER POS:

        up = enemyHitBox.getY() > playerYPos;
        down = enemyHitBox.getY() < playerYPos;
        left = enemyHitBox.getX() > playerXPos;
        right = enemyHitBox.getX() < playerXPos;



        //-----AVAIL PLAYER POSITION

        if (up && !isCollidingUP) {
            enemyHitBox.translate(0, -1);
            enemy1.translate(0,-1);
        }

        if (right && !isCollidingRIGHT) {
            enemyHitBox.translate(1, 0);
            enemy1.translate(1,0);
        }

        if (down && !isCollidingDOWN) {
            enemyHitBox.translate(0, 1);
            enemy1.translate(0,1);
        }

        if (left && !isCollidingLEFT) {
            enemyHitBox.translate(-1, 0);
            enemy1.translate(-1,0);
        }

    }

    //-----------------------------------------COLLISION METHODS--------------------------------------------------------

    @Override
    public boolean isMovingUp() {
        return up;
    }

    @Override
    public boolean isMovingDown() {
        return down;
    }

    @Override
    public boolean isMovingLeft() {
        return left;
    }

    @Override
    public boolean isMovingRight() {
        return right;
    }

    @Override
    public void setCollidingUP(boolean value) {
        isCollidingUP = value;
    }

    @Override
    public void setCollidingDOWN(boolean value) {
        isCollidingDOWN = value;
    }

    @Override
    public void setCollidingLEFT(boolean value) {
        isCollidingLEFT = value;
    }

    @Override
    public void setCollidingRIGHT(boolean value) {
        isCollidingRIGHT = value;
    }

    @Override
    public Rectangle getHitBox() {
        return enemyHitBox;
    }

    public boolean isDead() {
        return dead;
    }


    //------------------------------------------------------------------------------------------------------------------
}
