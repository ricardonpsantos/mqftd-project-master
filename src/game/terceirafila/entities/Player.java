package game.terceirafila.entities;


import game.terceirafila.Game;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player extends Entity {

    private int health;
    private boolean dead;
    private Rectangle playerHitBox;
    private Game game;
    private Direction firingDirection;
    private boolean firingState;
    private long fireTimeStamp;
    private long fireCadence;

    //--------------------------------------HERO PICTURES---------------------------------------------------------------

    private Picture activeImage;
    private Picture hero;
    private Picture heroMoveUp;
    private Picture heroShootDown;
    private Picture heroShootLeft;
    private Picture heroShootRight;
    private Picture heroShootUp;


    //--------------------------------MOVEMENT BOOLEAN PROPERTIES-------------------------------------------------------

    private boolean up;
    private boolean down;
    private boolean right;
    private boolean left;

    //------------------------------------COLLISION BOOLEAN PROPERTIES--------------------------------------------------

    private boolean isCollidingUP;
    private boolean isCollidingDOWN;
    private boolean isCollidingLEFT;
    private boolean isCollidingRIGHT;

    //------------------------------------------------------------------------------------------------------------------

    public Player(int health, Game game) {
        this.health = health;
        this.dead = false;
        this.game = game;
        this.firingState = false;
        this.fireCadence = 350000000;

    }

    public void pictureDraw(String value) {

        switch (value) {

            case "standing":
                this.hero = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero.gif");
                hero.draw();
                break;

            case "up":
                this.heroMoveUp = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero_move_up.gif");
                heroMoveUp.draw();
                break;

            case "shootUp":
                this.heroShootUp = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero_shoot_up.gif");
                heroShootUp.draw();
                break;

            case "shootDown":
                this.heroShootDown = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero_shoot_down.gif");
                heroShootDown.draw();
                break;

            case "shootLeft":
                this.heroShootLeft = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero_shoot_left.gif");
                heroShootLeft.draw();
                break;

            case "shootRight":
                this.heroShootRight = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero_shoot_right.gif");
                heroShootRight.draw();
                break;
        }


    }

    public void playerDraw(int spawnX, int spawnY) {
        playerHitBox = new Rectangle(spawnX, spawnY, 64, 64);
        this.hero = new Picture(getHitBox().getX(), getHitBox().getY(), "resources/hero.gif");
        hero.draw();
        activeImage = hero;

    }


    // TODO: 11/10/2019 Implement damage mechanics!
    public void getHit(int damage) {
        health -= damage;

        if (health <= 0) {
            die();
        }
    }

    public void die() {
        playerHitBox.delete();
        hero.delete();
        dead = true;
    }

    public void move() {

        if (up && !isCollidingUP) {
            playerHitBox.translate(0, -2);
            activeImage.translate(0, -2);
        }

        if (right && !isCollidingRIGHT) {
            playerHitBox.translate(2, 0);
            activeImage.translate(2, 0);
        }

        if (down && !isCollidingDOWN) {
            playerHitBox.translate(0, 2);
            activeImage.translate(0, 2);
        }

        if (left && !isCollidingLEFT) {
            playerHitBox.translate(-2, 0);
            activeImage.translate(-2, 0);
        }
    }

    //-------------------------------------------------FIRE METHODS-----------------------------------------------------

    public Bullet fire() {
        int x = playerHitBox.getX() + playerHitBox.getWidth() / 2;
        int y = playerHitBox.getY() + playerHitBox.getHeight() / 2;

        // TODO: 11/10/2019 If there is time, review asset recycling process.
        /*for (Bullet bullet : game.getListOfBullets()) {

            if (bullet.isSpent()) {

                bullet.resetBullet(x, y, firingDirection);
                return bullet;
            }
        }*/

        Bullet newBullet = new Bullet(x, y, firingDirection);
        newBullet.draw();
        fireTimeStamp = System.nanoTime();
        return newBullet;
    }

    public boolean canFire() {
        return System.nanoTime() > fireTimeStamp + fireCadence;
    }

    public void setFiringState(boolean value) {
        firingState = value;
    }

    public void setFiringDirection(Direction value) {
        firingDirection = value;
    }

    public boolean getFiringState() {
        return firingState;
    }

    //--------------------------------MOVEMENT SETTERS------------------------------------------------------------------

    public void setUp(boolean value) {
        up = value;
    }

    public void setDown(boolean value) {
        down = value;
    }

    public void setRight(boolean value) {
        right = value;
    }

    public void setLeft(boolean value) {
        left = value;
    }

    //------------------------------------------------------------------------------------------------------------------


    //-----------------------------------------------------COLLISION METHODS--------------------------------------------

    public boolean isMovingUp() {
        return up;
    }

    public boolean isMovingDown() {
        return down;
    }

    public boolean isMovingLeft() {
        return left;
    }

    public boolean isMovingRight() {
        return right;
    }

    @Override
    public void setCollidingUP(boolean value) {
        isCollidingUP = value;
    }

    @Override
    public void setCollidingDOWN(boolean value) {
        isCollidingDOWN = value;
    }

    @Override
    public void setCollidingLEFT(boolean value) {
        isCollidingLEFT = value;
    }

    @Override
    public void setCollidingRIGHT(boolean value) {
        isCollidingRIGHT = value;
    }

    @Override
    public Rectangle getHitBox() {
        return playerHitBox;
    }

    public void setActiveImage(Picture image) {
        activeImage = image;
    }
    //------------------------------------------------------------------------------------------------------------------

    public boolean getDead() {
        return dead;
    }

    public void setDead(boolean value) {
        dead = value;
    }


    public Picture getHeroStanding() {
        return hero;
    }

    public Picture getHeroMoveUp() {
        return heroMoveUp;
    }

    public Picture getGetHeroShootUp() {
        return heroShootUp;
    }

    public Picture getHeroShootDown() {
        return heroShootDown;
    }

    public Picture getHeroShootLeft() {
        return heroShootLeft;
    }

    public Picture getHeroShootRight() {
        return heroShootRight;
    }

    public Picture getActiveImage() {
        return activeImage;
    }
}
